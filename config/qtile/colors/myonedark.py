#Colors
background_light = ["#07070700", "#07070700"]
background_dark = ["#07070700", "#07070700"]
foreground = ["#CCCAC2", "#CCCAC2"]
foregroundvague = ["#707A8C", "#707A8C"]

red = ["#ff7566", "#ff7566"]
green = ["#a9dc76", "#a9dc76"]
blue = ["#78dce8", "#78dce8"]
yellow = ["#ffd866", "#ffd866"]
orange = ["#fc9867", "#fc9867"]
purple = ["#ff6188", "#ff6188"]

b = blue
